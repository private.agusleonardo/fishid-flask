from __future__ import division
import sys
sys.path.append('../')
import os
import cv2
import numpy as np
import sys
import pickle
import time
from tensorflow import ConfigProto
from tensorflow import InteractiveSession
from keras import backend as K
from keras_frcnn import config
from keras.layers import Input
from keras.models import Model
from keras_frcnn import roi_helpers
from keras.applications.mobilenet import preprocess_input
from keras_frcnn import resnet as nn
import tensorflow as tf

def load_model(basePath):
	config = ConfigProto()
	config.gpu_options.allow_growth = True
	session = tf.Session(config=config) #from InteractiveSession
	sys.setrecursionlimit(50000)

	global modelPath
	modelPath = basePath+"\\model.hdf5"

	global configPath
	configPath = basePath+"\\config.pickle"

	global model_rpn
	global model_classifier
	global C
	global graph
	global rpn_layers
	global classifier
	global shared_layers
	global num_rois
	global class_mapping

	num_features = 1024

	with open(configPath, 'rb') as f_in:
		C = pickle.load(f_in)

	C.network = 'resnet50'
	C.use_horizontal_flips = False
	C.use_vertical_flips = False
	C.rot_90 = False
	C.num_rois = 10
	class_mapping = C.class_mapping

	if 'bg' not in class_mapping:
		class_mapping['bg'] = len(class_mapping)

	class_mapping = {v: k for k, v in class_mapping.items()}
	print(class_mapping)

	input_shape_img = (None, None, 3)
	input_shape_features = (None, None, num_features)

	img_input = Input(shape=input_shape_img)
	roi_input = Input(shape=(C.num_rois, 4))
	feature_map_input = Input(shape=input_shape_features)

	# define the base network (resnet here, can be VGG, Inception, etc)
	shared_layers = nn.nn_base(img_input)

	# define the RPN, built on the base layers
	num_anchors = len(C.anchor_box_scales) * len(C.anchor_box_ratios)
	rpn_layers = nn.rpn(shared_layers, num_anchors)
	classifier = nn.classifier(feature_map_input, roi_input, C.num_rois, nb_classes=len(class_mapping))

	model_rpn = Model(img_input, rpn_layers)
	model_classifier = Model([feature_map_input, roi_input], classifier)

	#changed from 0.5 to 0.8 by martin
	bbox_threshold = 0.5

	num_rois = C.num_rois

	# model loading
	model_rpn.load_weights(modelPath, by_name=True)
	model_classifier.load_weights(modelPath, by_name=True)

	model_rpn._make_predict_function()
	model_classifier._make_predict_function()

	#model_rpn.compile(optimizer='adam', loss='mse')
	#model_classifier.compile(optimizer='adam', loss='mse')

	graph = tf.get_default_graph()

def get_detection(imgData):
	global model_rpn
	global model_classifier
	global graph
	global C
	global num_rois
	global class_mapping
	
	all_imgs = []
	classes = {}

	img = cv2.imdecode(imgData, cv2.IMREAD_COLOR)
	
	# preprocess image
	X, ratio = format_img(img, C)
	img_scaled = (np.transpose(X[0,:,:,:],(1,2,0))).astype('uint8')
	X = np.transpose(X, (0, 2, 3, 1))

	# get the feature maps and output from the RPN
	with graph.as_default():
		[Y1, Y2, F] = model_rpn.predict(X)

	#changed from 0.3 to 0.7 by martin
	R = roi_helpers.rpn_to_roi(Y1, Y2, C, K.image_dim_ordering(), overlap_thresh=0.7)
	
	# convert from (x1,y1,x2,y2) to (x,y,w,h)
	R[:, 2] -= R[:, 0]
	R[:, 3] -= R[:, 1]

	# apply the spatial pyramid pooling to the proposed regions
	bboxes = {}
	probs = {}

	for jk in range(R.shape[0]//num_rois + 1):
		ROIs = np.expand_dims(R[num_rois*jk:num_rois*(jk+1),:],axis=0)
		if ROIs.shape[1] == 0:
			break

		if jk == R.shape[0]//num_rois:
			#pad R
			curr_shape = ROIs.shape
			target_shape = (curr_shape[0],num_rois,curr_shape[2])
			ROIs_padded = np.zeros(target_shape).astype(ROIs.dtype)
			ROIs_padded[:,:curr_shape[1],:] = ROIs
			ROIs_padded[0,curr_shape[1]:,:] = ROIs[0,0,:]
			ROIs = ROIs_padded

		[P_cls,P_regr] = model_classifier.predict([F, ROIs])

		for ii in range(P_cls.shape[1]):
			if np.argmax(P_cls[0,ii,:]) == (P_cls.shape[2] - 1): # jika probabilitas tertinggi adalah class background
				# jika probabilitas > 60% (cukup tinggi), maka kemungkinan memang background, abaikan
				if np.max(P_cls[0,ii,:]) > 0.3:
					continue
			elif np.max(P_cls[0,ii,:]) < 0.7: # jika probabilitas tertinggi bukan class background(class ikan) tapi probnya dibawah 75%, abaikan
				continue

			cls_name = class_mapping[np.argmax(P_cls[0,ii,:])]

			if cls_name not in bboxes:
				bboxes[cls_name] = []
				probs[cls_name] = []
			(x,y,w,h) = ROIs[0,ii,:]

			bboxes[cls_name].append([16*x,16*y,16*(x+w),16*(y+h)])
			probs[cls_name].append(np.max(P_cls[0,ii,:]))

	all_fish_bboxes = {}
	all_fish_bboxes["fish"] = []
	all_bg_bboxes = {}
	all_bg_bboxes["bg"] = []
	all_bg_prob = {}
	all_bg_prob["bg"] = []

	all_dets = []
	# terapkan fast NMS untuk hilangkan deteksi class yang double-double
	for key in bboxes:
		bbox = np.array(bboxes[key]) # ambil bbox untuk setiap class per iterasi, simpan dalam bentuk array [ [bbox1_x1, bbox1_y1, , bbox1_x2, , bbox1_y2], [bbox2_x1, bbox2_y1, , bbox2_x2, , bbox2_y2] ]
		prob = np.array(probs[key]) # ambil prob bbox untuk setiap class per iterasi, simpan dalam bentuk array [ bbox1_prob, bbox2_prob ]

		new_boxes, new_probs = roi_helpers.non_max_suppression_fast(bbox, prob, overlap_thresh = 0.25)
		if(key=="bg"):
			print("background")
			for bg_box in new_boxes:
				all_bg_bboxes["bg"].append(bg_box)
				
			for bg_prob in new_probs:
				all_bg_prob["bg"].append(bg_prob)
		else:
			print("fish")
			for fish_box in new_boxes:
				all_fish_bboxes["fish"].append(fish_box)

			for jk in range(new_boxes.shape[0]):
				(x1, y1, x2, y2) = new_boxes[jk,:]
				(real_x1, real_y1, real_x2, real_y2) = get_real_coordinates(ratio, x1, y1, x2, y2)
				all_dets.append([key, 100*new_probs[jk], real_x1, real_y1, real_x2, real_y2])

	# terapkan fast NMS bg untuk hilangkan background yang bertimpaan dengan class lain
	new_bg_boxes, new_bg_probs = roi_helpers.non_max_suppression_fast_background(np.array(all_bg_bboxes["bg"]), np.array(all_bg_prob["bg"]), np.array(all_fish_bboxes["fish"]), overlap_thresh = 0.3)
	print(new_bg_boxes)
	for i in range(len(new_bg_boxes)):
		(x1, y1, x2, y2) = new_bg_boxes[i][:]
		(real_x1, real_y1, real_x2, real_y2) = get_real_coordinates(ratio, x1, y1, x2, y2)
		all_dets.append(["unidentified_fish", 100*new_bg_probs[i], real_x1, real_y1, real_x2, real_y2])
	
	return all_dets

def format_img_size(img, C):
	""" formats the image size based on config """
	img_min_side = float(C.im_size)
	(height,width,_) = img.shape
		
	if width <= height:
		ratio = img_min_side/width
		new_height = int(ratio * height)
		new_width = int(img_min_side)
	else:
		ratio = img_min_side/height
		new_width = int(ratio * width)
		new_height = int(img_min_side)

	img = cv2.resize(img, (new_width, new_height), interpolation=cv2.INTER_CUBIC)
	return img, ratio	

def format_img_channels(img, C):
	#convert BGR array to RGB array

	img = img[:, :, (2, 1, 0)]
	img = img.astype(np.float32)
	img[:, :, 0] -= C.img_channel_mean[0]
	img[:, :, 1] -= C.img_channel_mean[1]
	img[:, :, 2] -= C.img_channel_mean[2]
	img /= C.img_scaling_factor
	img = np.transpose(img, (2, 0, 1))
	img = np.expand_dims(img, axis=0)
	return img

def format_img(img, C):
	""" formats an image for model prediction based on config """
	img, ratio = format_img_size(img, C)
	img = format_img_channels(img, C)
	return img, ratio

# Method to transform the coordinates of the bounding box to its original size
def get_real_coordinates(ratio, x1, y1, x2, y2):
	real_x1 = int(round(x1 // ratio))
	real_y1 = int(round(y1 // ratio))
	real_x2 = int(round(x2 // ratio))
	real_y2 = int(round(y2 // ratio))

	return (real_x1, real_y1, real_x2 ,real_y2)