import psycopg2

class MyPostgreSQL():
	# def __init__(self, host="127.0.0.1", database="dborinest", user="postgres", password="orinest"):
	def __init__(self, host="127.0.0.1", database="fishId", user="postgres", password="161110170"):
		self.conn = psycopg2.connect(host=host, database=database, user=user, password=password)
        # Create cursor
		self.cur = self.conn.cursor()

	def query(self, query):
		self.cur.execute(query)

	def fetchall(self):
		return self.cur.fetchall()

	def commit(self):
		self.conn.commit()

	def close(self):
		self.cur.close()
		self.conn.close()