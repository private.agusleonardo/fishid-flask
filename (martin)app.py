from __future__ import division
import sys
from flask import Flask, request, render_template, redirect, url_for, jsonify
from config import connection, folder
from datetime import datetime
from werkzeug.utils import secure_filename
import json
import re
import cv2
import sqlite3
import json
import pickle
import time
import base64
import io
import keras
import predictor
import numpy as np
import os
import array
from os import path

ALLOWED_EXTENSIONS = set(["png", "jpg", "jpeg"])


def allowed_file(filename):
    return "." in filename and filename.rsplit(".", 1)[1].lower() in ALLOWED_EXTENSIONS


app = Flask(__name__, static_url_path="", static_folder="static")

predictor.load_model("C:\\Project\\TA\\server\\Model\\01.Model_one")

app.config["BASE_URL"] = "/api"
app.config["PROFILE_FOLDER"] = folder.PROFILE_FOLDER
app.config["POST_FOLDER"] = folder.POST_FOLDER

# .\venv\Scripts\activate.bat


@app.route("{}/test".format(app.config["BASE_URL"]), methods=["GET"])
def test():
    return "<h1>BERHASIL</h1>", 200


@app.route("{}/register".format(app.config["BASE_URL"]), methods=["POST"])
def register():
    form = request.form
    userName = form["username"]
    password = form["password"]
    email = form["email"]
    db = connection.MyPostgreSQL()
    db.query(
        "INSERT INTO \"public\".\"user\" (username, password, email) VALUES('{}','{}','{}')".format(
            userName, password, email
        )
    )
    db.commit()
    db.query(
        'SELECT user_id, username, email, profile_pict FROM "public"."user" WHERE username = \'{}\''.format(
            userName
        )
    )
    result = db.fetchall()
    db.close()
    container = {
        "data": {
            "user_id": result[0][0],
            "username": result[0][1],
            "email": result[0][2],
            "profile_pict": f"/img/profile/{result[0][3]}",
        }
    }
    return json.dumps(container), 200


@app.route("{}/login".format(app.config["BASE_URL"]), methods=["POST"])
def login():
    form = request.form
    userName = form["username"]
    password = form["password"]
    db = connection.MyPostgreSQL()
    db.query(
        'SELECT user_id, username, password, email, profile_pict FROM "public"."user" WHERE username = \'{}\''.format(
            userName
        )
    )
    result = db.fetchall()
    db.close()

    if len(result) > 0:
        if result[0][2] == password:
            container = {
                "data": {
                    "user_id": result[0][0],
                    "username": result[0][1],
                    "email": result[0][3],
                    "profile_pict": f"/img/profile/{result[0][4]}",
                }
            }
            return json.dumps(container), 200
        else:
            db.close()
            return json.dumps({"message": "password anda salah"}), 422
    else:
        db.close()
        return json.dumps({"message": "userid tidak terdaftar"}), 422


@app.route("{}/upload".format(app.config["BASE_URL"]), methods=["POST"])
def upload():
    a = request.files.getlist("file")
    print(a)
    for item in a:
        print(item)
        print(type(item))
    return "ok", 200


@app.route(
    "{}/profile/<string:userId>".format(app.config["BASE_URL"]),
    methods=["GET", "POST"],
)
def profile(userId):
    if request.method == "POST":
        print(request.files)
        if "profile" not in request.files:
            return json.dumps({"message": "wrong parameters"}), 404
        file = request.files["profile"]

        if file.filename == "":
            return json.dumps({"message": "No selected file"}), 404

        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            file.save(os.path.join(app.config["PROFILE_FOLDER"], filename))
            form = request.form
            username = form["username"]
            email = form["email"]
            db = connection.MyPostgreSQL()
            db.query(
                "update \"public\".\"user\" set username = '{}', email = '{}', profile_pict = '{}' where user_id = '{}'".format(
                    username, email, filename, userId
                )
            )
            db.commit()
            db.close()
            return (
                json.dumps(
                    {
                        "data": {
                            "user_id": userId,
                            "username": username,
                            "email": email,
                            "profile_pict": "img/profile/{}".format(filename),
                        },
                        "message": "Change Profile Success",
                    }
                ),
                200,
            )
    elif request.method == "GET":
        db = connection.MyPostgreSQL()
        db.query(
            'SELECT user_id, username, email, profile_pict FROM "public"."user" WHERE user_id = \'{}\''.format(
                userId
            )
        )
        resultProfile = db.fetchall()
        db.query(
            'SELECT post.post_id, image FROM public.post INNER JOIN public.user ON post.user_id = "user".user_id where "user".user_id = {} order by post.post_id desc'.format(
                userId
            )
        )
        resultPost = db.fetchall()
        db.close()

        container = {
            "data": {
                "user": {
                    "user_id": resultProfile[0][0],
                    "username": resultProfile[0][1],
                    "email": resultProfile[0][2],
                    "profile_pict": f"/img/profile/{resultProfile[0][3]}",
                },
                "post": [],
            }
        }

        for item in resultPost:
            container["data"]["post"].append(
                {"post_id": item[0], "image": f"/img/post/{item[1]}"}
            )

        return json.dumps(container), 200


@app.route("{}/password".format(app.config["BASE_URL"]), methods=["POST"])
def password():
    userId = request.form["user_id"]
    oldPassword = request.form["old_password"]
    newPassword = request.form["new_password"]
    db = connection.MyPostgreSQL()
    db.query(
        'SELECT password from "public"."user" where user_id = \'{}\''.format(userId)
    )
    result = db.fetchall()
    if result[0][0] == oldPassword:
        db.query(
            "UPDATE \"public\".\"user\" set password = '{}' where user_id = '{}'".format(
                newPassword, userId
            )
        )
        db.commit()
        db.close()
        return json.dumps({"message": "password berhasil diganti"}), 200
    else:
        db.close()
        return json.dumps({"message": "wrong password"}), 404

@app.route("{}/fish".format(app.config["BASE_URL"]), methods=["GET"])
def getFishList():
    db = connection.MyPostgreSQL()
    db.query("SELECT * from fish")
    result = db.fetchall()

    container = {"data": []}
    for fish in result:
        container["data"].append(
            {"fish_id": fish[0], "nama_latin": fish[1], "nama_inggris": fish[2], "deskripsi": fish[3], "fish_picture": f"/img/fish/{fish[4]}", "bentuk_tubuh": fish[5], "bentuk_mulut": fish[6], "bentuk_sirip": fish[7], "bentuk_ekor": fish[8], "corak_tubuh": fish[9]} 
        )

    return json.dumps(container), 200

@app.route("{}/explore/<string:fishId>".format(app.config["BASE_URL"]), methods=["GET"])
def getExplore(fishId):
    db = connection.MyPostgreSQL()
    db.query("SELECT nama_latin, nama_inggris, deskripsi, fish_picture, bentuk_tubuh, bentuk_mulut, bentuk_sirip, bentuk_ekor, corak_tubuh from fish where fish_id = '{}'".format(fishId))
    resultFish = db.fetchall()
    db.query("SELECT post.post_id, image from post LEFT JOIN (SELECT DISTINCT post_id, fish_id FROM post_detail) A on A.post_id = post.post_id where A.fish_id = '{}'".format(fishId))
    resultPost = db.fetchall()
    db.close()

    container = {
            "data": {
                "fish": {
                    "nama_latin": resultFish[0][0],
                    "nama_inggris": resultFish[0][1],
                    "deskripsi": resultFish[0][2],
                    "fish_picture": f"/img/fish/{resultFish[0][3]}",
                    "bentuk_tubuh": resultFish[0][4],
                    "bentuk_mulut": resultFish[0][5],
                    "bentuk_sirip": resultFish[0][6],
                    "bentuk_ekor": resultFish[0][7],
                    "corak_tubuh": resultFish[0][8],
                },
                "post": [],
            }
        }

    for item in resultPost:
        container["data"]["post"].append(
            {"post_id": item[0], "image": f"/img/post/{item[1]}"}
        )

    return json.dumps(container), 200

@app.route("{}/detection".format(app.config["BASE_URL"]), methods=["POST"])
def get_detection_result():
    file = request.files["post"]
    encoded_string = ""
    decoded = ""
    encoded_string = base64.b64encode(file.read())
    decoded = base64.b64decode(encoded_string)

    nparr = np.fromstring(decoded, np.uint8)
    detection_result = predictor.get_detection(nparr)

    container = {"data": []}

    for detection in detection_result:
        db = connection.MyPostgreSQL()
        db.query(
            "SELECT fish_id, nama_latin, nama_inggris, bentuk_tubuh, bentuk_mulut, bentuk_sirip, bentuk_ekor, corak_tubuh from fish where nama_latin = '{}'".format(
                detection[0].capitalize().replace("_"," "),
            )
        )
        result = db.fetchall()
        
        if not result:
            container["data"].append(
                {"fish_id": "", "nama_latin": "", "nama_inggris": "", "bentuk_tubuh": "", "bentuk_mulut": "", "bentuk_sirip": "", "bentuk_ekor": "", "corak_tubuh": "","species": detection[0], "probability": detection[1], "x1": detection[2], "y1": detection[3], "x2": detection[4], "y2": detection[5]} 
            )
        else:
            container["data"].append(
                {"fish_id": result[0][0], "nama_latin": result[0][1], "nama_inggris": result[0][2], "bentuk_tubuh": result[0][3], "bentuk_mulut": result[0][4], "bentuk_sirip": result[0][5], "bentuk_ekor": result[0][6], "corak_tubuh": result[0][7], "species": detection[0], "probability": detection[1], "x1": detection[2], "y1": detection[3], "x2": detection[4], "y2": detection[5]} 
            )
            
    waktu = datetime.today().strftime("%Y-%m-%d %H:%M:%S")
    db.query("INSERT INTO scan (user_id, waktu) VALUES ('{}','{}')".format(userId,waktu))
    db.commit()

    db.query("SELECT scan_id from scan order by scan_id desc")
    result = db.fetchall()

    scanId = result[0][0]
    tempFishId = 0
    fishCount = 0
    index = 0

    for item in container["data"]:
        if(item["fish_id"] != ""):
            if tempFishId == 0:
                tempFishId = item["fish_id"]
                fishCount += 1
            elif item["fish_id"] != tempFishId:
                db.query("INSERT INTO scan_detail (scan_id, fish_id, jumlah) VALUES ('{}','{}','{}')".format(scanId, tempFishId, fishCount))
                db.commit()
                tempFishId = item["fish_id"]
                fishCount = 1
            else:
                fishCount += 1
            
            if index == len(container["data"]) -1:
                db.query("INSERT INTO scan_detail (scan_id, fish_id, jumlah) VALUES ('{}','{}','{}')".format(scanId, tempFishId, fishCount))
                db.commit()
        index+=1

    db.close()

    return json.dumps(container), 200

@app.route("{}/post".format(app.config["BASE_URL"]), methods=["POST"])
def post():
    print(request.files)
    if "post" not in request.files:
        return json.dumps({"message": "wrong parameters"}), 404
    file = request.files["post"]

    if file.filename == "":
        return json.dumps({"message": "No selected file"}), 404

    if file and allowed_file(file.filename):
        filename = secure_filename(file.filename)
        file.save(os.path.join(app.config["POST_FOLDER"], filename))

        userId = request.form["user_id"]
        caption = request.form["caption"]
        bentukTubuh = request.form["bentuk_tubuh"]
        bentukMulut = request.form["bentuk_mulut"]
        bentukSirip = request.form["bentuk_sirip"]
        bentukEkor = request.form["bentuk_ekor"]
        corakTubuh = request.form["corak_tubuh"]
        tanggal = datetime.today().strftime("%Y-%m-%d %H:%M:%S")
        fishId = request.form.getlist("fish_id")
        x1 = request.form.getlist("x1")
        y1 = request.form.getlist("y1")
        x2 = request.form.getlist("x2")
        y2 = request.form.getlist("y2")
        probability = request.form.getlist("probability")
        status = request.form.getlist("status")
        
        print("TEST PRINT")
        print(fishId)

        db = connection.MyPostgreSQL()
        db.query(
            "INSERT INTO \"public\".\"post\" (caption, tanggal, image, bentuk_tubuh, bentuk_mulut, bentuk_sirip, bentuk_ekor, corak_tubuh, user_id) VALUES('{}','{}','{}','{}','{}','{}','{}','{}','{}')".format(
                caption,
                tanggal,
                filename,
                bentukTubuh,
                bentukMulut,
                bentukSirip,
                bentukEkor,
                corakTubuh,
                userId
            )
        )
        db.commit()

        db.query('SELECT post_id from "post" order by post_id desc')
        result = db.fetchall()

        for i in range(len(fishId)):
            db.query(
                "INSERT INTO post_detail (post_id, fish_id, x1, y1, x2, y2, probability, status) VALUES('{}','{}','{}','{}','{}','{}','{}','{}')".format(
                    result[0][0],
                    fishId[i],
                    x1[i],
                    y1[i],
                    x2[i],
                    y2[i],
                    probability[i],
                    status[i]
                )
            )
            db.commit()
        db.close()

        encoded_string = ""
        decoded = ""
        with open(os.path.join(app.config["POST_FOLDER"], filename), "rb") as image_file:
            encoded_string = base64.b64encode(image_file.read())
            decoded = base64.b64decode(encoded_string)

        nparr = np.fromstring(decoded, np.uint8)
        detection_result = predictor.get_detection(nparr)
        response = {"data":[{"species": detection[0], "probability": detection[1], "x1": detection[2], "y1": detection[3], "x2": detection[4], "y2": detection[5]} for detection in detection_result]}

        return json.dumps(response), 200
    else:
        return json.dumps({"message": "No file found or wrong extension"}), 404




@app.route("{}/home/<string:userId>".format(app.config["BASE_URL"]), methods=["GET"])
def home(userId):
    db = connection.MyPostgreSQL()
    db.query(
        'SELECT post.post_id, caption, image, post.bentuk_tubuh, post.bentuk_mulut, post.bentuk_sirip, post.bentuk_ekor, post.corak_tubuh, post.user_id, "user".username, "user".profile_pict, "like".status, A.jumlah, nama_latin, x1, y1, x2, y2, probability FROM public.post INNER JOIN public.user ON post.user_id = "user".user_id LEFT JOIN "like" ON "like".post_id = post.post_id and "like".user_id = {} LEFT JOIN (Select count(*) as jumlah, post_id from "like" where status = True group by post_id) A on post.post_id = A.post_id LEFT JOIN post_detail on "post".post_id = post_detail.post_id LEFT JOIN fish on post_detail.fish_id = fish.fish_id where post_detail.status = true order by post.post_id desc'.format(
            userId
        )
    )
    result = db.fetchall()
    db.close()

    container = {"data": []}
    for item in result:
        container["data"].append(
            {
                "post_id": item[0],
                "caption": item[1],
                "image": f"/img/post/{item[2]}",
                "bentuk_tubuh": item[3],
                "bentuk_mulut": item[4],
                "bentuk_sirip": item[5],
                "bentuk_ekor": item[6],
                "corak_tubuh": item[7],
                "user_id": item[8],
                "username": item[9],
                "profile_pict": f"/img/profile/{item[10]}",
                "like_status": checkNullBool(item[11]),
                "like_count": checkNullInt(item[12]),
                "species": item[13],
                "x1": item[14],
                "y1": item[15],
                "x2": item[16],
                "y2": item[17],
                "probability": item[18],
            }
        )

    return json.dumps(container), 200


@app.route("{}/like".format(app.config["BASE_URL"]), methods=["POST"])
def like():
    postId = request.form["post_id"]
    userId = request.form["user_id"]

    db = connection.MyPostgreSQL()
    db.query(
        'SELECT post_id from "like" where user_id = {} and post_id = {}'.format(
            userId, postId
        )
    )
    result = db.fetchall()
    if len(result) > 0:
        db.query(
            "UPDATE \"like\" SET status = '{}' where user_id = {} and post_id = {}".format(
                True, userId, postId
            )
        )
    else:
        db.query(
            "INSERT INTO \"like\" VALUES('{}','{}','{}')".format(postId, userId, True)
        )
    db.commit()
    db.close()
    return json.dumps({"message": "post berhasil"}), 200


@app.route("{}/unlike".format(app.config["BASE_URL"]), methods=["POST"])
def unlike():
    postId = request.form["post_id"]
    print(postId)
    userId = request.form["user_id"]

    db = connection.MyPostgreSQL()
    db.query(
        'SELECT post_id from "like" where user_id = {} and post_id = {}'.format(
            userId, postId
        )
    )
    result = db.fetchall()
    if len(result) > 0:
        db.query(
            "UPDATE \"like\" SET status = '{}' where user_id = {} and post_id = {}".format(
                False, userId, postId
            )
        )
    else:
        db.query(
            "INSERT INTO \"like\" VALUES('{}','{}','{}')".format(postId, userId, False)
        )
    db.commit()
    db.close()
    return json.dumps({"message": "post berhasil"}), 200

@app.route("{}/report".format(app.config["BASE_URL"]), methods=["POST"])
def report():
    postId = request.form["post_id"]
    userId = request.form["user_id"]
    reason = request.form["reason"]

    db = connection.MyPostgreSQL()
    db.query(
        "INSERT INTO report (post_id, user_id, reason, status) VALUES('{}','{}','{}','{}')".format(postId, userId, reason, True)
    )
    db.commit()
    db.close()
    return json.dumps({"message": "post berhasil"}), 200

@app.route("{}/post/delete".format(app.config["BASE_URL"]), methods=["POST"])
def delete():
    postId = request.form["post_id"]
    userId = request.form["user_id"]

    db = connection.MyPostgreSQL()
    db.query(
        'DELETE from "post" where post_id = {}'.format(postId)
    )
    db.commit()
    db.close()
    return json.dumps({"message": "post berhasil"}), 200


@app.route("{}/comment/<string:postId>".format(app.config["BASE_URL"]), methods=["GET"])
def getComment(postId):
    db = connection.MyPostgreSQL()
    db.query(
        'select "user".username, "user".profile_pict, "comment", waktu from "comment" inner join "user" on "user".user_id = "comment".user_id and "comment".post_id = {} order by "comment".waktu asc'.format(
            postId
        )
    )
    result = db.fetchall()
    db.close()

    container = {"data": []}
    for item in result:
        container["data"].append(
            {
                "username": item[0],
                "profile_pict": f"/img/profile/{item[1]}",
                "comment": item[2],
                "waktu": "{}".format(item[3]),
            }
        )

    return json.dumps(container), 200


@app.route("{}/comment".format(app.config["BASE_URL"]), methods=["POST"])
def postComment():
    postId = request.form["post_id"]
    userId = request.form["user_id"]
    comment = request.form["comment"]
    waktu = request.form["waktu"]

    db = connection.MyPostgreSQL()
    db.query(
        "INSERT INTO \"comment\" VALUES('{}','{}','{}','{}')".format(
            postId, userId, comment, waktu
        )
    )

    db.commit()
    db.close()
    return json.dumps({"message": "post berhasil"}), 200


@app.route(
    "{}/post/<string:userId>/<string:postId>".format(app.config["BASE_URL"]),
    methods=["GET"],
)
def profilePost(userId, postId):
    db = connection.MyPostgreSQL()
    db.query(
        'SELECT post.post_id, caption, image, bentuk_tubuh, bentuk_mulut, bentuk_sirip, bentuk_ekor, corak_tubuh, post.user_id, "user".username, "user".profile_pict, "like".status, A.jumlah FROM public.post INNER JOIN public.user ON post.user_id = "user".user_id LEFT JOIN "like" ON "like".post_id = post.post_id and "like".user_id = {} LEFT JOIN (Select count(*) as jumlah, post_id from "like" where status = True group by post_id) A on post.post_id = A.post_id where post.post_id = {}'.format(
            userId, postId
        )
    )
    result = db.fetchall()
    db.close()

    container = {
        "data": {
            "post_id": result[0][0],
            "caption": result[0][1],
            "image": f"/img/post/{result[0][2]}",
            "bentuk_tubuh": result[0][3],
            "bentuk_mulut": result[0][4],
            "bentuk_sirip": result[0][5],
            "bentuk_ekor": result[0][6],
            "corak_tubuh": result[0][7],
            "user_id": result[0][8],
            "username": result[0][9],
            "profile_pict": f"/img/profile/{result[0][10]}",
            "like_status": checkNullBool(result[0][11]),
            "like_count": checkNullInt(result[0][12]),
        }
    }

    return json.dumps(container), 200


@app.route(
    "{}/collection/like/<string:userId>".format(app.config["BASE_URL"]),
    methods=["GET"],
)
def collectionLike(userId):
    db = connection.MyPostgreSQL()

    db.query(
        'SELECT post.post_id, image FROM public.post INNER JOIN "user" ON post.user_id = "user".user_id INNER JOIN "like" ON "like".post_id = "post".post_id WHERE "user".user_id = {} and "like".status = true order by post.post_id desc'.format(
            userId
        )
    )
    resultPost = db.fetchall()
    db.close()

    container = {"data": {"post": [],}}

    for item in resultPost:
        container["data"]["post"].append(
            {"post_id": item[0], "image": f"/img/post/{item[1]}"}
        )

    return json.dumps(container), 200


@app.route(
    "{}/collection/history/<string:userId>".format(app.config["BASE_URL"]),
    methods=["GET"],
)
def collectionHistory(userId):
    db = connection.MyPostgreSQL()

    db.query(
        "select scan.scan_id, fish.nama_latin, scan_detail.jumlah, scan.waktu from scan inner join scan_detail on scan.scan_id = scan_detail.scan_id inner join fish on scan_detail.fish_id = fish.fish_id where scan.user_id = {} order by scan.waktu desc".format(
            userId
        )
    )
    resultPost = db.fetchall()
    db.close()

    container = {"data": []}

    scanId = 0
    for item in resultPost:
        if scanId != item[0]:
            container["data"].append(
                {"scan_id": item[0], "fish": [], "waktu": "{}".format(item[3])}
            )

        container["data"][len(container["data"]) - 1]["fish"].append(
            {"nama_latin": item[1], "jumlah": item[2]}
        )

        scanId = item[0]

    return json.dumps(container), 200


def checkNullBool(item):
    if item == None:
        return False
    else:
        return item


def checkNullInt(item):
    if item == None:
        return 0
    else:
        return item


if __name__ == "__main__":
    # app.run(debug=True, host="192.168.43.19", port="5000")
    app.run(debug=True, host="0.0.0.0", port="5000")
